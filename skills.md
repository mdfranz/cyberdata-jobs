# Summary of Roles
- Intelligence, CyberSecurity. Fraud 
- Operational, Consulting, and Product Development

# Job Classes

## Strategic Analyst
- Data consumption & reporting focused. Sometimes visualization
- Strategic Cross-functional
- Likely in mature organizations
- High industry or domain knowlege

Examples: Tiktok, CITI, Amazon, JP Morgan, LexisNexis

## Strategic Scientist
- Algorithm development & prototyping
- Usually statistics 
- Dissemination of 
- Software engineering and tool development
- Moderate domain knowledge

Examples: Mass Mutual, Vectra, LLNL, DiDi, Guidehouse, Square, Netflix

## Engineer 
- Software engineering, cloud, and data infrastructure
- Low to Moderate domain knowledge
- ETL

Examples: XOR, CACI, JP Morgan

## Generalist, All the Things!
- Both engineering and data science

Examples, F5, Affirm, Cofense

# Questions
- What is the depth & breadth of skills, knowledge, and experience required?
- How detailed & specific are the technical requirements? What can we infer about tech stacks?
- How much Cyber, risk, or fraud domain knowledge is required for the role? 

# Data Analysis Tools & Techniques
- SAS, R
- Numpy, Scipy, Matploltlib, Scikit-Learn, Pytorch, Keras, xgbuoost
- Qualitative & Quantitative 
- Graph & Network Analysis 
- Pattern recognition & False Positive Reduction 

# Big Data / Data Warehousing
- Spark, Hbase, Hadoop, Map-Reduce, EMR
- Oracle, Teradata
- MQ, Kafka, Hive, Impala 
- Redshift, BigQuery
- Streaming Analytics

# Statistical Approaches & Algorithms
- Bayesian
- NLP
- MATLAB/Octave
- Anomaly detection, deep learning
- Nueral Networks
- Gradient boosting, Markov decision processes
- Logistal regression 
- Computer vision 

# Communication & Visualization
- "Robust vizualizations & clear explanations" 
- Interactive tooling & dashboards
- Reporting (data, executive, project) 
- D3, Dash, Tableux, PowerBI, Shiny, Kibana

# Data Platform
- Pipelines Ingestion, cleansing, and schedulers
- MongoDB, Elasticsearch, Lucene
- Postgres, MySQL, JDBC, ODBC
- Schema Design
- Scheduling & ETL (Airflows, Autosys, Control-M)
- Model deployment 

# Software Engineering
- Python, R, Scala, Node.js,  Java, C++, C#
- Microservices architecture,
- DDD, Even Sources, CQRS
- Git & friends 
- JSON, XML, and Regular Expressions 
- Web frameworks (Flask, Django) & Javascript
- REST APIs 
- Agile Practices and SDLC

# Cyber & Fraud
- SIEM, IDS, DLP, UEBA, Anti-Fraud 
- Penetration Testing
- Malware Analysis, Yara, 
- Exploit Vulnerabilities Scanning (Nmap, Nessus, Shodan)
- Phishing (BEC, EAC), Account takoever, money laundering
- Mitre ATT&CK
- Adversarial TTPs
- Browser finger priting
- Device identification and authentication 
- Adversary techniques (polymorphism, impersonation, obfuscation) 
- Fraud and Social Engineering (Gift card, Wire transfer)

# Platform Engineering
- Cloud infrastructure (GCP, AWS, Azure) 
- DevOps, CICD (Terraform, etc.)
- Container orchestration (Docker, Kubernetes) 
- Logging (Splunk, ELK) 
- Object Storage (S3, Google Storage)
- Lambda, Cloud Functions, etc. 
- System 
